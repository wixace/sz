﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIPages : MonoBehaviour {

   
    [SerializeField] private GameObject[] _list;

    [SerializeField] private string[] _titles;

    [SerializeField] private Button _nextBtn, _prevBtn;

    [SerializeField] private UnityEvent  _onPageEnd;

    [SerializeField] private OnPageChanged _onPageChanged;

    [SerializeField] private bool _isLoop;

    [SerializeField] private bool _saveLastSession;

    [SerializeField] private Text _titleText;

    public int Index { get; private set; }

    void Start() {
        _nextBtn?.onClick.AddListener(Next);
        _prevBtn?.onClick.AddListener(Previous);
    }

    void OnEnable() {
        _list[Index].gameObject.SetActive(true);
        UpdateTitle();
    }

    public void Next() => UpdatePage(1);

    public void Previous() => UpdatePage(-1);

    private void UpdatePage(int step) {
        if (_isLoop) {
            _list[Index].gameObject.SetActive(false);
        } else if (Index + step > _list.Length - 1) {
            _onPageEnd?.Invoke();
            return;
        }

        if (Index + step > _list.Length - 1) {
            Index = 0;
        } else if (Index + step < 0)
            Index = _list.Length - 1;
        else
            Index += step;
        _list[Index].gameObject.SetActive(true);
        UpdateTitle();
        _onPageChanged?.Invoke(Index);
    }

    void UpdateTitle() {
        if (_titles.Length > 0)
            _titleText.text = _titles[Index];
    }

    void OnDisable() {
        if (!_saveLastSession)
            Index = 0;
        _list[Index].gameObject.SetActive(false);

    }

    [System.Serializable]
    private class OnPageChanged:UnityEvent<int>{}


}
