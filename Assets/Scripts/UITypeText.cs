﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UITypeText : MonoBehaviour {


    [SerializeField] private string _content;
    [SerializeField] private float _interval = .3f;
    private float _startInterval;

    private int _index;
    private Text _text;

    public bool IsPlaying { get; set; }

    private Action callback;

    void Awake() {
        _text = GetComponent<Text>();
        _startInterval = _interval;
    }

    public void OnPointerClick(PointerEventData eventData) {
        if (eventData.clickCount == 2) {
            ShowImmediate();
        }
    }

    public void SetText(string str, Action callback = null) {
        IsPlaying = true;
        _content = str;
        _text.text = string.Empty;
        _index = 0;
        this.callback = callback;
        StopAllCoroutines();
        StartCoroutine(ShowText());
    }

    public void ShowImmediate() {
        IsPlaying = false;
        _interval = 0;
    }

    private IEnumerator ShowText() {
        int count = 0;
        while (_text.text.Length <= _content.Length + count) {
            try {
                var t = _content[_index++];
                if (t.ToString() == @"\") {
                    _text.text += Environment.NewLine;
                    count++;
                } else
                    _text.text += t;
            } catch (Exception e) {
                break;
            }
            yield return new WaitForSeconds(_interval);
        }
        _interval = _startInterval;
        IsPlaying = false;
        if (callback != null)
            callback.Invoke();
    }
}
