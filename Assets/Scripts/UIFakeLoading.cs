﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIFakeLoading : MonoBehaviour {

    [SerializeField] private AnimationCurve _animationCurve;
    [SerializeField] private Text _progressText;
    [SerializeField] private Image _progressBar;
    [SerializeField] private UnityEvent _onDone;

    private float _startTime;
    private float _percentage;


    private void OnEnable() {
        _startTime = Time.time;
    }

    // Update is called once per frame
    void Update() {
        _percentage = Mathf.Clamp01(_animationCurve.Evaluate(Time.time - _startTime));
        _progressText.text = Mathf.RoundToInt(_percentage * 100) + "%";
        _progressBar.fillAmount = _percentage;
        if (_percentage >= 1) {
            _onDone?.Invoke();
            gameObject.SetActive(false);
        }
    }

}
