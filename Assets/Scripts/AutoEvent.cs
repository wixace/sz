﻿using UnityEngine;
using UnityEngine.Events;

public class AutoEvent : MonoBehaviour {

    [SerializeField] private UnityEvent _event;

    [SerializeField] private float _delay;

    public void Execute() {
        _event?.Invoke();
        enabled = false;
    }

    private void OnEnable() {
        Invoke("Execute", _delay);
    }


}

