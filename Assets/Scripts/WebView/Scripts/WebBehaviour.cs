﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
#if  UNITY_IOS
using UnityEngine.iOS;
#endif
public class WebBehaviour : MonoBehaviour {
	[SerializeField] private GameObject ChangelogView, ErrorView,         WebCover;
	[SerializeField] private Text       UpdateTitle,   UpdateText,        ErrorText;
	[SerializeField] private Button     PrivacyButton, UpdateCloseButton, RetryButton;

	[SerializeField] private string _webScene = "Web";

	private string[] _url = {
		"https://api2.bmob.cn/1/classes/List/blcj888C",
		"https://sdk.panguhy.com/game/config?channel=400"
	};

	private string _applicationId="2951c3124cc60bb8a7b7f3a1633f7118";
	private string _restApiKey = "334978adf870154f765c4e30cfd03d24";
	
	private Data   _data;
	private string _idfa,           _idfv;
	private int    _currentVersion, _currentUrlIndex;

	protected virtual void OnEnable() => Connect();

	void Start() {
		RetryButton.onClick.AddListener(() => {
			ErrorView.SetActive(false);
			Connect();
		});
	}

#region Private Methods

	#if UNITY_IOS
	private void InitIosUserProperty() {
		Application.RequestAdvertisingIdentifierAsync(
		                                              (string advertisingId, bool trackingEnabled, string error) => {
			                                              Debug.Log("advertisingId=" + advertisingId + " enabled=" +
			                                                        trackingEnabled + " error=" + error);
			                                              _idfa     = advertisingId;
			                                              _idfv     = Device.vendorIdentifier;
			                                              _data.url = string.Format(_data.url, _idfa, _idfv);
			                                              //WebView.Url = _data.url;
			                                              PlayOnlineGame();
			                                              // WKWebView.Instance.Load(_data.url);
		                                              }
		                                             );
	}
#endif
		#if UNITY_ANDROID
    	private void InitAndroidUserProperty() {
	        _data.url = "http://sdk.panguhy.com/game/direct_login?pgcid=2&gameId=3&imei=";
	     //   _data.url = string.Format(_data.url, SystemInfo.deviceUniqueIdentifier);
        }
#endif

	private void ShowChangelog() {
		WebCover.SetActive(false);
		ChangelogView.SetActive(true);
	}


	private void HideChangelog() => ChangelogView.SetActive(false);

	private void PlayOfflineGame() {
		HideChangelog();
		WebCover.SetActive(false);
		AudioManager.Instance.Play();
	}

	private void PlayOnlineGame() {
		WebView.Url = _data.url;
		HideChangelog();
		AudioManager.Instance.Pause();
		SceneManager.LoadScene(_webScene);
	}

#endregion

	public void Connect() {
		if (Application.internetReachability != NetworkReachability.NotReachable) {
			ErrorView.SetActive(false);
			_currentVersion = PlayerPrefs.GetInt("Version", 0);
			StartCoroutine(GetData(data => {
				_data         = data;
				_data.urlEnable = true;
				PlayerPrefs.SetInt("Version", _data.version);
				PlayerPrefs.Save();
				if (_data.urlEnable) {
					#if UNITY_IOS
					InitIosUserProperty();
					#elif UNITY_ANDROID
					InitAndroidUserProperty();
					#elif UNITY_EDITOR
					PlayOnlineGame();
					#endif
					return;
				}

				if (_data.updateEnable) {
					if (_currentVersion != _data.version || _data.forceUpdate) {
						UpdateText.text  = _data.updateText;
						UpdateTitle.text = _data.updateTitle;
						PrivacyButton.onClick.AddListener(() => { Application.OpenURL(_data.privacyUrl); });
						UpdateCloseButton.onClick.AddListener(() => {
							HideChangelog();
							PlayOfflineGame();
						});
						ShowChangelog();
					}
					else {
						HideChangelog();
						PlayOfflineGame();
					}
				}
				else {
					PlayOfflineGame();
				}
			}));
		}
		else {
			ErrorView.SetActive(true);
		}
	}

	IEnumerator GetData(Action<Data> onSuccess) {
		switch (_currentUrlIndex) {
			case 0:
				var headers = new Dictionary<string, string>();
				headers.Add("X-Bmob-Application-Id", _applicationId);
				headers.Add("X-Bmob-REST-API-Key", _restApiKey);
				var www = new WWW(_url[_currentUrlIndex], null, headers);
				yield return www;
				if (!string.IsNullOrEmpty(www.text)) {
					var dat = JsonUtility.FromJson<Data>(www.text);
					onSuccess(dat);
				}
				else {
					HideChangelog();
					if (_currentUrlIndex < _url.Length - 1)
						_currentUrlIndex++;
					Connect();
				}

				break;
			default:
				UnityWebRequest uwr = UnityWebRequest.Get(_url[_currentUrlIndex]);
				yield return uwr.Send();
				if (uwr.isNetworkError || uwr.isHttpError) {
					Debug.Log(uwr.error);
					_currentUrlIndex++;
					if (_currentUrlIndex >= _url.Length) {
						_currentUrlIndex = 0;
						ErrorView.SetActive(true);
						ErrorText.text = "服务器连接失败,请稍后重试";
					}
					else {
						Connect();
					}
				}
				else {
					Debug.Log(System.Text.RegularExpressions.Regex.Unescape(uwr.downloadHandler.text));
					var dat = JsonUtility.FromJson<Data>(uwr.downloadHandler.text);
					onSuccess?.Invoke(dat);
				}

				break;
		}
	}

	[System.Serializable]
	private class Data {
		public int    version;      //版本号不一致时显示更新弹窗
		public bool   urlEnable;    //链接开关
		public bool   updateEnable; //开启更新弹窗
		public bool   forceUpdate;  //无视版本号显示更新弹窗
		public string url;          //链接
		public string privacyUrl;   //隐私政策地址
		public string updateTitle;  //更新弹窗标题
		public string updateText;   //更新弹窗内容
	}
}