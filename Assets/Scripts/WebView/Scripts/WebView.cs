﻿using UnityEngine;

public class WebView : USingleton<WebView> {
	
	WebViewObject webViewObject;
	public static string Url { get; set; }="http://sdk.panguhy.com/game/direct_login?pgcid=2&gameId=3&imei=";
	void Start() {
		Url += SystemInfo.deviceUniqueIdentifier;
		webViewObject = (new GameObject("WebViewObject")).AddComponent<WebViewObject>();
		webViewObject.Init(
		                   cb: (msg) => { Debug.Log(string.Format("CallFromJS[{0}]", msg)); },
		                   err: (msg) => { Debug.Log(string.Format("CallOnError[{0}]", msg)); },
		                   started: (msg) => { Debug.Log(string.Format("CallOnStarted[{0}]", msg)); },
		                   ld: (msg) => {
			                   webViewObject.SetVisibility(true);
		                   },enableWKWebView:true,transparent:true);
		webViewObject.LoadURL(Url);
		webViewObject.SetVisibility(true);
	}
	
}