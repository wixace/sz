﻿using UnityEngine;
using UnityEngine.UI;

public class UIPagesNextBtn : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => {
            GetComponentInParent<UIPages>().Next();
        });
    }

  
}
