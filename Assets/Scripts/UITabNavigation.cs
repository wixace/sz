﻿using UnityEngine;

public class UITabNavigation : MonoBehaviour {

	[SerializeField]
	private GameObject _selectedItem;

    public void ShowTab(GameObject obj) {


		if(!gameObject.activeSelf)
			gameObject.SetActive(true);
	   //if( !obj.activeSelf)
		  // obj.SetActive(true);
	    if (_selectedItem != obj)
	    {
		    _selectedItem?.SetActive(false);
		    _selectedItem = obj;
	    }
	    obj.SetActive(true);
	}

}
