﻿using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class StoryManager : MonoBehaviour {

    private TextAsset _storySheet;

    [SerializeField] private string _playerName;

    [SerializeField] private Sprite _playerSprite;

    [SerializeField] private GameObject _textDialog;

    [SerializeField] private UITypeText _typeText;

    [SerializeField] private Text _roleName;

    [SerializeField] private Image _roleImage,_backgroundImage;

    [SerializeField] private SpriteConfig[] _roleConfig, _backgroundConfig;

    [SerializeField] private StoryEvent[] _storyEvent;

    private Story _storyModels;

    private StoryModel[] _currentStory;

    private int _storyIndex;

    private void Awake() {
        _storySheet = Resources.Load<TextAsset>("StoryJson");
        // _storyModels = CsvUtility.GetCsvRecords<StoryModel>(_storySheet.text).ToArray();
        _storyModels = JsonUtility.FromJson<Story>(_storySheet.text);
    }

    public void LoadStory(string storyId) {
        _storyIndex = -1;
        _currentStory =_storyModels.Stories.Where(i => i.Story == storyId).ToArray();
        _textDialog.SetActive(true);
        Next();
    }

    public void Next() {
        if (_typeText.IsPlaying)
            _typeText.ShowImmediate();
        else {
            _storyIndex++;
            if (_storyIndex == _currentStory.Length) {
                _textDialog.SetActive(false);
               var storyEvent=_storyEvent.FirstOrDefault((i => i.Id == _currentStory[0].Story));

                storyEvent.Event?.Invoke();
            } else {
                var story = _currentStory[_storyIndex];
                if (story.Name == _playerName) {
                    _roleImage.sprite = _playerSprite;
                } else {
                    if (!string.IsNullOrEmpty(story.Name)) {
                        _roleImage.sprite = _roleConfig.First(i => i.Name == story.Name).Sprite;
                    }
                }
                _typeText.SetText(story.Text);
                _roleName.text = story.Name;
            }
        }
    }

    public void SetPlayerSprite(Sprite sprite) => _playerSprite = sprite;

    [System.Serializable]
    private class Story {
        public StoryModel[] Stories;
    }

    [System.Serializable]
    private class StoryModel {
        public string Id;
        public string Story;
        public string Text;
        public string Name;
    }

    [System.Serializable]
    private struct SpriteConfig {
        public string Name;
        public Sprite Sprite;
    }

    [System.Serializable]
    private struct StoryEvent {
        public string Id;
        public UnityEvent Event;
    }
}
