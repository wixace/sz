﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIImageSelector : MonoBehaviour {

    [SerializeField] private Sprite[] _images;

    [SerializeField] private Button _nextBtn, _previousBtn;

    [SerializeField] private bool _isLoop;

    [SerializeField] private bool _saveLastSession;

    [SerializeField] private Image _image;

    [SerializeField] private UnityEvent _onPageEnd;

    [SerializeField] private OnImageSelect _onImageSelect;

    private int _index;

    void Start() {
        _nextBtn.onClick.AddListener(Next);
        _previousBtn.onClick.AddListener(Previous);
    }

    void OnEnable() => UpdateSprite();


    private void UpdateSprite() => _image.sprite = _images[_index];

    public void Select() => _onImageSelect?.Invoke(_images[_index]); 

    public void Next() {
        if (_index < _images.Length - 1) {
            _index++;
            UpdateSprite();
        } else {
            if (_isLoop) {
                _index = 0;
                UpdateSprite();
            } else
                _onPageEnd.Invoke();
        }
    }

    public void Previous() {
        if (_index > 0) {
            _index--;
            UpdateSprite();
        } else if (_isLoop) {
            _index = _images.Length - 1;
            UpdateSprite();
        }
    }

    void OnDisable() {
        if (!_saveLastSession) {
            _index = 0;
        }
    }

    [System.Serializable]
    private class OnImageSelect : UnityEvent<Sprite> { }
}
