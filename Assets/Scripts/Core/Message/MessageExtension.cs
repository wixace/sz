﻿using System;
using Fs.Core.Message;

namespace Fs.Extension
{
    public static class MessageExtension
    {

        public static object Publish<T>(this object sender, string name, T content)
        {
            var msg = new MessageArgs<T>(content);
            MessageAggregator<T>.Instance.Publish(name, sender, msg);
            return sender;
        }

        public static object Publish<T>(this object sender, string name, MessageArgs<T> args)
        {
            MessageAggregator<T>.Instance.Publish(name, sender, args);
            return sender;
        }

        public static void Subscribe<T>(this object receiver, string name, Action<T> act) => MessageAggregator<T>.Instance.Subscribe(name, (sender, args) => act(args.Item));

        public static void Subscribe<T>(this object receiver, Action<T> act) => MessageAggregator<T>.Instance.Subscribe(receiver.GetType().Name, (sender, args) => act(args.Item));

        public static void Unsubscribe<T>(this object receiver, string name) => MessageAggregator<T>.Instance.Unsubscribe(name);

    }
}
