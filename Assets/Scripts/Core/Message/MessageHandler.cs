﻿namespace Fs.Core.Message
{
    internal delegate void MessageHandler<T>(object sender, MessageArgs<T> args);
}