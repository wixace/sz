﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UINameButton : MonoBehaviour {

    [SerializeField] private InputField _inputField;

    [SerializeField] private string _dictionary;

    [SerializeField] private int _charLength;

    private Button _button;

    private void Awake() {
        _button = GetComponent<Button>();
    }

    private void Start() {
            _dictionary.Trim();
        _button.onClick.AddListener(() => {
            RandomName();
        });
    }

    void RandomName() {
        string result = string.Empty;
        for (int i = 0; i < _charLength; i++) {
            result += _dictionary[Random.Range(0, _dictionary.Length)];
        }
        _inputField.text = result;
    }
}
