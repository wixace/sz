﻿using UnityEngine;
using UnityEngine.UI;

public class UICloneImage : MonoBehaviour {

    [SerializeField] private Image _target;

    // Start is called before the first frame update
    private void OnEnable() {
        GetComponent<Image>().sprite = _target.sprite;
    }
}
